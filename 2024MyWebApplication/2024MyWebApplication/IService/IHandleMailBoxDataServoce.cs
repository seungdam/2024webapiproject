﻿using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.IService;
public interface IHandleMailBoxDataService
{
    public Task<(CSCommon.ErrorCode ErrorCode, SlotData NewSlotData)>   RecieveMail(Int32 user_id, Int32 mail_id);

    public Task<(CSCommon.ErrorCode ErrorCode, MailData[] MailBoxData)> LoadMailBox(Int32 user_id, Int32 mail_page);
}

﻿
namespace _2024MyWebApplication.IService;

public interface IHandleAccountDataService 
{
    Task<CSCommon.ErrorCode> RegisterAccount(String email, String orgin_pw);
    Task<CSCommon.ErrorCode> VerifyAccount(String email, String request_pw);
}

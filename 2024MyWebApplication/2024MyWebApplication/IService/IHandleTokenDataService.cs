﻿
namespace _2024MyWebApplication.IService;

public interface IHandleTokenDataService
{
    public Task<bool> RegisterUserAuthToken(String email, String token);
}
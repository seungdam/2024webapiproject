using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.IService;
public interface IHandleCharacterDataService
{
    public Task<(CSCommon.ErrorCode ErrorCode, CharacterData? ResultData)> LoadMyCharacterData(String Email);
}
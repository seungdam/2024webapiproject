﻿using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.IService;
public interface IHandleInventoryDataService
{
    
    public Task<(CSCommon.ErrorCode ErrorCode, SlotData[]? InventoryData)> LoadInventoryData(Int32 user_id,Int32 inventory_page);

    public Task<(CSCommon.ErrorCode ErrorCode, SlotData? NewSlotData)> AddItem(Int32 user_id, Int32 item_code, Int32 increase_count);

    public Task<CSCommon.ErrorCode> UserOrDropItem(Int32 user_id, Int32 slot_num, Int32 decrease_count);

}

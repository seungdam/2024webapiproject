﻿using System.ComponentModel.DataAnnotations;

namespace _2024MyWebApplication.Models.DataStructure;


// 내가 계정 생성할 때 사용할 모델 형태
public class AccountData
{
    [Required]
    public String UserEmail { get; set; }
    [Required]
    public String UserHashedPassword { get; set; }
}

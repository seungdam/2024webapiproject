﻿namespace _2024MyWebApplication.Models.DataStructure;

public class CharacterData
{

    public Int32 Id { get; set; }
    public Int32 Level { get; set; }
    public String Job { get; set; }
}

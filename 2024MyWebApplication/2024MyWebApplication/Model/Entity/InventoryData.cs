﻿namespace _2024MyWebApplication.Models.DataStructure;


public class SlotData
{
    public Int32 Index { get; set; }
    public Int32 ItemCode { get; set; }
    public Int32 ItemCount { get; set; }
}
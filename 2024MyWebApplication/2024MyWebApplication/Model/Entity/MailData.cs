﻿using System.ComponentModel.DataAnnotations;
using _2024MyWebApplication.Models.Validation;

namespace _2024MyWebApplication.Models.DataStructure;

public class MailData
{
    [Required]
    public Int32 MailIndex { get; set; }
    [Required]
    public Int32 ItemCode { get; set; }
    [Required]
    public Int32 ItemCount { get; set; }

    public DateTime RecievedDate { get; set; }
    public DateTime ExpirationDate { get; set; }
}

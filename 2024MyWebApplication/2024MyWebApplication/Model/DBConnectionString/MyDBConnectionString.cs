﻿namespace _2024MyWebApplication.Model.DBConnectionString;

public class MyDBConnectionString
{
        public string MySqlGameDataDb { get; set; }
        public string RedisTokenDataDb { get; set; }
        public string RedisRequestDb { get; set; }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;


namespace _2024MyWebApplication.Models.Validation;
public class DateTimeValidation : ActionFilterAttribute
{
    // Mail과 같은 DataModel의 경우 수신날짜가 만료날짜보다 이후의 날짜면 안된다. 
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        base.OnActionExecuting(context);
        var recv_date = context.ActionArguments["ItemRecievedDate"] as DateTime?;
        var expr_date = context.ActionArguments["ItemExpirationDate"] as DateTime?;

        if (recv_date.HasValue && expr_date.HasValue)
        {
            if(DateTime.Compare(recv_date.Value, expr_date.Value) > 0) // 만료 기간보다 수신 기간이 이후인 경우는 에러 반환
            {
                context.ModelState.AddModelError("MailDate", "MailDates is invaild.");
                var problem_details = new ValidationProblemDetails(context.ModelState)
                {
                    Status = StatusCodes.Status400BadRequest
                };

                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
        else // 데이트 값들은 필수 항목임을 체크
        {
            context.ModelState.AddModelError("MailDate","MailDates doesn't exist.");
            var problem_details = new ValidationProblemDetails(context.ModelState)
            {
                Status = StatusCodes.Status404NotFound
            };
            context.Result = new NotFoundObjectResult(context.ModelState);
        }

    }
}

﻿using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.Model.IRepository;

public interface ICachedInventoryDataRepository
{
    public Task<CSCommon.ErrorCode> CacheInventoryData(Int32 user_id, SlotData[] slot_dats);

    public Task<CSCommon.ErrorCode> IncreaseItem(Int32 user_id, Int32 slot_num, Int32 increase_cnt);
    public Task<CSCommon.ErrorCode> DecreaseItem(Int32 user_id, Int32 slot_num, Int32 decrease_cnt);

}

﻿namespace _2024MyWebApplication.Model.IRepository;

public interface IUserRequestDataRepository : IDisposable
{
    public Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> SetUserRequestType(Int32 user_id, Int32 request_type);

    public void DeleteUserRequestType(Int32 user_id);
}
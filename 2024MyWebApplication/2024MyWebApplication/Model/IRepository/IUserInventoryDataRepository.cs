﻿using _2024MyWebApplication.Models.DataStructure;
using SqlKata.Execution;

namespace _2024MyWebApplication.Model.IRepository;

public interface IUserInventoryDataRepository : IDisposable
{

    public Task<(CSCommon.ErrorCode ErrorCode, SlotData[]? SlotDatas)> GetInventoryData(Int32 character_id, Int32 inventory_page);
    public Task<CSCommon.ErrorCode> UpdateInventoryData(Int32 character_id, Int32 inventory_page, SlotData[] final_slot_datas);
}

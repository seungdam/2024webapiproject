﻿using _2024MyWebApplication.Models.DataStructure;
using CSCommon;

namespace _2024MyWebApplication.Model.IRepository;


public interface IUserAccountDataRepository : IDisposable

{
    public Task<CSCommon.ErrorCode> InsertAccountData(String email, String hased_pw);
    public Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> IsExistedAccountData(String email);

    public Task<AccountData?> GetAccountData(String email);

}

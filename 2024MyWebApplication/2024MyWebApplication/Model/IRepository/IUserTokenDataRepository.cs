﻿using CloudStructures;
using CloudStructures.Structures;
using StackExchange.Redis;

namespace _2024MyWebApplication.Model.IRepository;

public interface IUserTokenDataRepository : IDisposable
{

    public Task<bool> SetAuthToken(String email, String token); // 토큰 추가하기

    public Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> IsExistAuthToken(String token);


}




using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.Model.IRepository;

public interface IUserCharacterDataRepository : IDisposable
{
    public Task<(CSCommon.ErrorCode ErrorCode, CharacterData QueryResult)> GetCharacterDatas(String user_email);
}

using _2024MyWebApplication.Models.DataStructure;

public interface IUserMailDataRepository : IDisposable
{
    public Task<(CSCommon.ErrorCode ErrorCode, MailData[] QueryResult)>  GetMailBoxData(Int32 user_id, Int32 mail_page);

    public Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> InsertMailData(Int32 user_id, Int32 mail_id);

    public Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> DeleteMailData(Int32 user_id, Int32 mail_id);
}
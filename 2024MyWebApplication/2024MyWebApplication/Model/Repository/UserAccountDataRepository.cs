﻿using _2024MyWebApplication.Model.DBConnectionString;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Models.DataStructure;
using CSCommon;
using Microsoft.Extensions.Options;
using MySqlConnector;
using SqlKata.Compilers;
using SqlKata.Execution;
using StackExchange.Redis;
using System.Data;
using ZLogger;

namespace _2024MyWebApplication.Model.Repository;




public class UserAccountDataRepository : IUserAccountDataRepository // 해당 Account 클래스는 MySql을 사용하므로 클래스명을 MySqlAccountDb라고 짓는다
{
    readonly ILogger<UserAccountDataRepository> _logger;
    readonly IUserTokenDataRepository _user_token_db;
    readonly IOptions<MyDBConnectionString> _db_config;

    Compiler         _sql_complier;
    QueryFactory     _sql_query_factory;
    IDbConnection    _sql_db_conn; 
    //  IDbConnection 또한 한가지 DB에 국한된 것이 아닌
    //  다양한 DBConnection을 사용할 수 있도록 하는 인터페이스이다.
    
    
    public UserAccountDataRepository(ILogger<UserAccountDataRepository> logger, IOptions<MyDBConnectionString> db_config)
    {
        this._logger = logger;
        _db_config = db_config;
        
        _sql_db_conn = new MySqlConnection(db_config.Value.MySqlGameDataDb);
        _sql_db_conn.Open();

        _sql_complier = new MySqlCompiler();
        _sql_query_factory =  new QueryFactory(_sql_db_conn,_sql_complier);
    }

 
    public async Task<CSCommon.ErrorCode> InsertAccountData(String email, String hashed_password)
    {
        var error_code = CSCommon.ErrorCode.ErrorNone;

        try
        {
            var query_result = await _sql_query_factory.Query("accountinfo").InsertAsync(new
            {
                email = email,
                hashed_password = hashed_password
            });

            if(query_result != 1)
            {
                _logger.ZLogError($"[UserAccountRepository.InsertAccountData] ErrorCode: {CSCommon.ErrorCode.MySqlQueryFailed}\n");
                error_code = CSCommon.ErrorCode.MySqlQueryFailed;
            }
            else
            {
                throw new Exception();
            }
        }
        catch
        {
            _logger.ZLogError($"[UserAccountRepository.InsertAccountData] ErrorCode: {CSCommon.ErrorCode.MySqlQueryErrorException}\n");
            error_code = CSCommon.ErrorCode.MySqlQueryErrorException;
        }

        return error_code;
    }

    public async Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> IsExistedAccountData(String email)
    {
        var error_code = CSCommon.ErrorCode.ErrorNone;
        try
        {
            var query_result = await _sql_query_factory.Query("accountinfo").Where("email", email).ExistsAsync();
            return (ErrorCode: error_code, QueryResult: query_result);
        }
        catch
        {
            _logger.ZLogError($"[UserAccountRepository.IsExistsAccountData] ErrorCode: {CSCommon.ErrorCode.MySqlQueryErrorException}\n");
            return (ErrorCode: error_code, QueryResult: false);
        }
    }
    
    public async Task<AccountData?> GetAccountData(String email)
    {
        try
        {
            var query_result = await _sql_query_factory.Query("accountinfo").Where("email", email).FirstAsync<AccountData>();
            return query_result;
        }
        catch
        {
            _logger.ZLogError($"[UserAccountRepository.GetAccountData] ErrorCode: {CSCommon.ErrorCode.MySqlQueryErrorException}\n");
            return null;
        }
    }

    public void Dispose()
    {
        _sql_db_conn.Close();
    }
}

﻿using _2024MyWebApplication.Model.DBConnectionString;
using _2024MyWebApplication.Model.IRepository;
using CloudStructures;
using CloudStructures.Structures;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using ZLogger;

namespace Com2usProject.Repository;

public class UserRequestDataRepository : IUserRequestDataRepository
{
    readonly ILogger<UserRequestDataRepository> _logger;
    readonly IOptions<MyDBConnectionString> _sql_db_config;


    RedisConnection _requset_repos_conn;
    RedisConfig     _request_repos_config;

    public UserRequestDataRepository(ILogger<UserRequestDataRepository> logger, IOptions<MyDBConnectionString> sql_db_config)
    {
        _logger = logger;
        _sql_db_config = sql_db_config;

        _request_repos_config = new RedisConfig("DbCommandRedisDb", _sql_db_config.Value.RedisRequestDb);
        _requset_repos_conn = new RedisConnection(_request_repos_config);
    }

    public async Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> SetUserRequestType(Int32 user_id, Int32 request_type)
    {

        var error_code = CSCommon.ErrorCode.ErrorNone;
        var query_result = false;
        try
        {
            // 카운팅이 필요한 상황이므로 String을 사용한다.
            var redis_string_key = new RedisString<Int32>(_requset_repos_conn, user_id.ToString(), TimeSpan.FromSeconds(2));
            query_result = await redis_string_key.SetAsync(request_type, when: When.NotExists);
            if (!query_result)
            {
                error_code = CSCommon.ErrorCode.RedisQeryFailed;
            }
        }
        catch
        {
            if(error_code == CSCommon.ErrorCode.ErrorNone)
            {
                error_code = CSCommon.ErrorCode.RedisQueryErrorException;
            }
            _logger.ZLogError($"[RedisDb.SetUserRequestType] ErrorCode: {error_code}");
   
        }

        return (ErrorCode: error_code, QueryResult: query_result);
    }

    public async void DeleteUserRequestType(int user_id)
    {
        try
        {
            var redisQuery = new RedisString<CSCommon.RequestTypeAsKey>(_requset_repos_conn, user_id.ToString(), null);
            var result = await redisQuery.DeleteAsync();
            if (!result)
            {
                
            }
        }
        catch
        {
            _logger.ZLogError($"[DeleteUserRequsetType.DeleteUserRequestType] Query Failed");
        }

    }

    public void Dispose()
    {
        _requset_repos_conn.GetConnection().Close();
    }
}

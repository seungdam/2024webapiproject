using Microsoft.Extensions.Options;
using SqlKata.Compilers;
using ZLogger;
using SqlKata.Execution;
using System.Data;
using MySqlConnector;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Model.DBConnectionString;
using _2024MyWebApplication.Models.DataStructure;

namespace Com2usProject.Repository;

public class UserCharacterDataRepository : IUserCharacterDataRepository
{
    readonly ILogger<UserInventoryDataRepository> _logger;
    readonly IOptions<MyDBConnectionString> _db_config;

    Compiler _sql_complier;
    QueryFactory _sql_query_factory;
    IDbConnection _sql_db_conn;

    public UserCharacterDataRepository(ILogger<UserInventoryDataRepository> logger, IOptions<MyDBConnectionString> dbconfig)
    {
        _logger = logger;
        _db_config = dbconfig;
        
        _sql_db_conn = new MySqlConnection(dbconfig.Value.MySqlGameDataDb);
        _sql_db_conn.Open();
        _sql_complier = new MySqlCompiler();
        _sql_query_factory = new QueryFactory(_sql_db_conn, _sql_complier);

    }
    public async Task<(CSCommon.ErrorCode ErrorCode, CharacterData? QueryResult)> GetCharacterDatas(String email)
    {
        var error_code = CSCommon.ErrorCode.ErrorNone;
        try
        {
            var query_result = await _sql_query_factory.Query("player_info")
                                                .Select("user_id", "job", "level")
                                                .Where("user_email", email)
                                                .FirstOrDefaultAsync<CharacterData>();
            if(query_result is null)
            {
                error_code = CSCommon.ErrorCode.MySqlQueryFailed;
            }
            return (ErrorCode: error_code, QueryResult: query_result);
        }
        catch
        {
            error_code = CSCommon.ErrorCode.MySqlQueryErrorException;
            _logger.ZLogError($"[IUserCharacterDataRepository]: {error_code}");
            return (ErrorCode: error_code, QueryResult: null);
        }

       
    }

    public void Dispose()
    {
        _sql_db_conn.Close();
    }
}

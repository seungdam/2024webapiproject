﻿using Microsoft.Extensions.Options;
using StackExchange.Redis;
using CloudStructures;
using CloudStructures.Structures;
using CSCommon;
using ZLogger;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Model.DBConnectionString;

namespace _2024MyWebApplication.Model.Repository;

public class UserTokenDataRepository : IUserTokenDataRepository
{
    readonly ILogger<UserTokenDataRepository> _logger;
    readonly IOptions<MyDBConnectionString> _sql_db_config;

    RedisConnection _token_repos_conn;
    RedisConfig _token_repos_config;

    public UserTokenDataRepository(ILogger<UserTokenDataRepository> logger, IOptions<MyDBConnectionString> db_config)
    {
        _logger = logger;
        _sql_db_config = db_config;

        try
        {
            _token_repos_config = new RedisConfig("AuthTokenRedisDb", db_config.Value.RedisTokenDataDb);
            _token_repos_conn = new RedisConnection(_token_repos_config);
        }
        catch
        {
           _logger.ZLogError($"[UserTokenDataRepository] {ErrorCode.RedisConnErrorException}");
        }

    }

    public async Task<(CSCommon.ErrorCode ErrorCode, bool QueryResult)> IsExistAuthToken(String token)
    {

        try
        {
            var token_string_value = new RedisString<String>(_token_repos_conn, token, null);
            var query_result = await token_string_value.ExistsAsync(); // 토큰이 존재하는가?
            return (ErrorCode: CSCommon.ErrorCode.ErrorNone, QueryResult: query_result);
        }
        catch
        {

            _logger.ZLogError($"[UserTokenDataRepository.IsExistAuthTocken] {CSCommon.ErrorCode.RedisQueryErrorException}");
            return (ErrorCode: CSCommon.ErrorCode.RedisQueryErrorException, QueryResult: false);
        }
        
    }

    public async Task<bool> SetAuthToken(String email, String token)
    {
        try
        {
            var token_string_value = new RedisString<String>(_token_repos_conn, token, null);
            var query_result = await token_string_value.SetAsync(email);

            if (!query_result)
            {
                _logger.ZLogInformation($"[UserTokenDataRepository.SetAuthToken] {CSCommon.ErrorCode.RedisQeryFailed}");
            }

            return query_result;
        }
        catch
        {
            _logger.ZLogError($"[UserTokenDataRepository.SetAuthToken] {CSCommon.ErrorCode.RedisQueryErrorException}");
            return false;   
        }
    }

    public void Dispose()
    {
        _token_repos_conn.GetConnection().Close();
    }
}

﻿
using Microsoft.Extensions.Options;
using MySqlConnector;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Data;
using ZLogger;
using CloudStructures.Structures;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Model.DBConnectionString;
using _2024MyWebApplication.Models.DataStructure;

namespace Com2usProject.Repository;

public class UserInventoryDataRepository : IUserInventoryDataRepository
{
    readonly ILogger<UserInventoryDataRepository> _logger;
    readonly IOptions<MyDBConnectionString> _db_config;

    Compiler _sql_complier;
    QueryFactory _sql_query_factory;
    IDbConnection _sql_db_conn;


    public UserInventoryDataRepository(ILogger<UserInventoryDataRepository> logger, IOptions<MyDBConnectionString> dbconfig)
    {
        _logger = logger;
        _db_config = dbconfig;
        _sql_db_conn = new MySqlConnection(dbconfig.Value.MySqlGameDataDb);
        _sql_db_conn.Open();


        _sql_complier = new MySqlCompiler();
        _sql_query_factory = new QueryFactory(_sql_db_conn, _sql_complier);
    }

    public async Task<(CSCommon.ErrorCode ErrorCode, SlotData[]? SlotDatas)> GetInventoryData(Int32 user_id, Int32 inventory_page)
    {
        try
        {
            // 최대 9칸 까지만 가져온다.
            var query_result = await _sql_query_factory.Query("inventoryinfo")
                                .Select("slot_num", "item_code", "item_count")
                                .Where("user_id", user_id)
                                .OrderBy("slot_num")
                                .ForPage(inventory_page, perPage: 9)
                                .GetAsync<SlotData>();
                                
            if (query_result is not null)
            {
                return (ErrorCode: CSCommon.ErrorCode.ErrorNone, SlotDatas: query_result.ToArray());
            }
            else
            {
                return (ErrorCode: CSCommon.ErrorCode.MySqlQueryFailed, SlotDatas: null);
            }
        }
        catch
        {
            _logger.ZLogError($"[UserInventoryDataRepository] ErrorCode: {CSCommon.ErrorCode.MySqlQueryErrorException}");
            return (ErrorCode: CSCommon.ErrorCode.MySqlQueryErrorException, SlotDatas: null);
        }
    }

   // 플레이어가 게임 종료 시,
   public async Task<CSCommon.ErrorCode> UpdateInventoryData(Int32 user_id, Int32 inventory_page, SlotData[] slot_datas)
   {

        try
        {
            foreach (var slot in slot_datas)
            {
                // 아직 까진 sqlkata에서 다중 레코드에 관한 업데이트를 지원 X --> 저장 프로시저로 변경할 필요가 있음
                var query_result = await _sql_query_factory
                                        .Query("inventoryinfo")
                                        .Select("item_code", "item_count")
                                        .Where("user_id", user_id)
                                        .UpdateAsync
                                        (
                                            new
                                            {
                                                slot_num = slot.Index,
                                                item_code = slot.ItemCode,
                                                item_count = slot.ItemCount
                                            }
                                        );
            }

            return CSCommon.ErrorCode.ErrorNone;
        }
        catch
        {
            _logger.ZLogError($"[UserInventoryDataRepository] ErrorCode: {CSCommon.ErrorCode.MySqlQueryErrorException}");
            return CSCommon.ErrorCode.MySqlQueryErrorException;
        }
   }

  
    public void Dispose()
    {
        _sql_db_conn.Close();
    }
}

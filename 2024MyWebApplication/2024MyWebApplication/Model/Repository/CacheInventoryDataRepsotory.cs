﻿using _2024MyWebApplication.Model.DBConnectionString;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Models.DataStructure;
using CloudStructures;
using CloudStructures.Structures;
using CSCommon;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using ZLogger;

namespace _2024MyWebApplication.Model.Repository;

public class SlotValue
{
    public Int32 ItemCode { get; set; }
    public Int32 ItemCount { get; set; }
}

public class CachedInventoryDataRepsotory : ICachedInventoryDataRepository
{

    readonly ILogger<CachedInventoryDataRepsotory> _logger;
    readonly IOptions<MyDBConnectionString> _sql_db_config;

    RedisConnection _repos_conn;
    RedisConfig     _repos_config;


    public CachedInventoryDataRepsotory(ILogger<CachedInventoryDataRepsotory> logger, IOptions<MyDBConnectionString> db_config)
    {
        _logger = logger;
        _sql_db_config = db_config;

        try
        {
            _repos_config = new RedisConfig("AuthTokenRedisDb", db_config.Value.RedisTokenDataDb);
            _repos_conn = new RedisConnection(_repos_config);

           
        }
        catch
        {
            _logger.ZLogError($"[UserTokenDataRepository] {ErrorCode.RedisConnErrorException}");
        }

    }

    public async Task<CSCommon.ErrorCode> CacheInventoryData(Int32 character_id, SlotData[] slot_datas)
    {

        try
        {
            var key = $"user_inventory:{character_id}";
            var inventory_dictionary = new RedisDictionary<Int32, SlotValue>(_repos_conn, key, TimeSpan.FromDays(1));
            var query_reuslt = true;
            foreach (var slot in slot_datas)
            {
                var field = slot.Index;
                var value = new SlotValue() { ItemCode = slot.ItemCode, ItemCount = slot.ItemCount};
                query_reuslt &= await inventory_dictionary.SetAsync(field, value, default, When.NotExists);
            }

            if (query_reuslt)
            {
                _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] {CSCommon.ErrorCode.RedisQeryFailed}");
                return CSCommon.ErrorCode.RedisQeryFailed;
            }
            else
            {
                return CSCommon.ErrorCode.ErrorNone;
            }
        }
        catch
        {
            _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] {CSCommon.ErrorCode.RedisQueryErrorException}");
            return CSCommon.ErrorCode.RedisQueryErrorException;
        }
    }
    public async Task<CSCommon.ErrorCode> IncreaseItem(Int32 character_id, Int32 slot_num, Int32 increase_count)
    {
        var key = $"user_inventory {character_id}";
        var field = slot_num;

        try
        {
            var inventory_dictionary = new RedisDictionary<Int32, SlotValue>(_repos_conn, key, TimeSpan.FromDays(1));
            var get_result = await inventory_dictionary.GetAsync(field);
  
            if (get_result.HasValue)
            {
                get_result.Value.ItemCount += increase_count;
                await inventory_dictionary.SetAsync(slot_num, get_result.Value);

                return CSCommon.ErrorCode.ErrorNone;
            }
            else
            {
                _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] ErrorCode: {CSCommon.ErrorCode.RedisQeryFailed}");
                return CSCommon.ErrorCode.RedisQeryFailed;
            }
        }
        catch
        {
            _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] {CSCommon.ErrorCode.RedisQueryErrorException}");
            return CSCommon.ErrorCode.RedisQueryErrorException;
        }
    }
    public async Task<CSCommon.ErrorCode> DecreaseItem(Int32 character_id, Int32 slot_num, Int32 decrease_countt)
    {
        var key = $"user_inventory {character_id}";
        var field = slot_num;

        try
        {
            var inventory_dictionary = new RedisDictionary<Int32, SlotValue>(_repos_conn, key, TimeSpan.FromDays(1));
            var get_result = await inventory_dictionary.GetAsync(field);

            if (get_result.HasValue)
            {
                get_result.Value.ItemCount -= decrease_countt;
                await inventory_dictionary.SetAsync(slot_num, get_result.Value);
                return CSCommon.ErrorCode.ErrorNone;
            }
            else
            {
                _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] {CSCommon.ErrorCode.RedisQeryFailed}");
                return CSCommon.ErrorCode.RedisQeryFailed;
            }
           
        }
        catch
        {
            _logger.ZLogError($"[CacheInventoryRepository.IncreaseItem] {CSCommon.ErrorCode.RedisQueryErrorException}");
            return CSCommon.ErrorCode.RedisQueryErrorException;
        }

    }
}

﻿using _2024MyWebApplication.Models.DataStructure;
using System.ComponentModel.DataAnnotations;


namespace _2024MyWebApplication.Models.ReqResStructure;

public class GetInventoryDataReq : BaseReq
{
    public Int32 InventoryPage { get; set; }
}

public class GetInventoryDataRes : BaseRes
{
    [Required]
    public SlotData[] InventoryData { get; set; }
}

public class UpdateInventoryDataReq : BaseReq
{
    public Int32 SlotNumber { get; set; }
    public Int32 ItemCode { get; set; }
    public Int32 ItemCount { get; set; }
}

public class UpdateInventoryDataRes : BaseRes
{
    [Required]
    public SlotData SlotData { get; set; }
}
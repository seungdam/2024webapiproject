﻿using System.ComponentModel.DataAnnotations;

namespace _2024MyWebApplication.Models.ReqResStructure;

public class GetMailBoxDataReq : BaseReq
{
    [Required]
    public Int32 MailBoxPage { get; set; }
}
public class GetMailBoxDataRes : BaseRes
{
    [Required]
    public String MailBoxInfo { get; set; }
}


public class UpdateMailBoxDataReq : BaseReq
{
    [Required]
    public Int32 MailId { get; set; }
}

public class UpdateMailBoxDataRes : BaseRes
{
    [Required]
    public Int32  DelMailId { get; set; }
    [Required]
    public String NewSlotData { get; set; }

}
using System.ComponentModel.DataAnnotations;

namespace _2024MyWebApplication.Models.ReqResStructure;

public class BaseReq
{

    [Required]
    public String UserToken { get; set; }
    [Required]
    public Int32 UserId { get; set; }
    [Required]
    public CSCommon.RequestTypeAsKey RequestType { get; set; }

}

public class BaseRes
{
    public CSCommon.ErrorCode ErrorCode { get; set; }
}
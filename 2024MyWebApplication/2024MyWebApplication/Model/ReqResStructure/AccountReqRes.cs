﻿using _2024MyWebApplication;
using _2024MyWebApplication.Models.DataStructure;
using System.ComponentModel.DataAnnotations;

namespace _2024MyWebApplication.Models.ReqResStructure;

public class RegisterAccountReq
{

    [Required]
    [MinLength(1, ErrorMessage = "EMAIL CANNOT BE EMPTY")]
    [StringLength(50, ErrorMessage = "EMAIL IS TOO LONG")]
    [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
    [DataType(DataType.EmailAddress)]
    public String Email { get; set; }

    [Required]
    [MinLength(1, ErrorMessage = "PASSWORD CANNOT BE EMPTY")]
    [StringLength(10, ErrorMessage = "PASSWORD IS TOO LONG")]
    [DataType(DataType.Password)]
    public String Password { get; set; }
}

public class RegisterAccountRes : BaseRes
{
    
}

public class LoginAccountRes : BaseRes
{
    [Required]
    public String UserToken { get; set; }
}

public class LoadCharacterReq : BaseReq
{
    [Required]
    public String UserEmail { get; set; }
}
public class LoadCharacterRes : BaseRes
{
    [Required]
    public CharacterData? CharacterData { get; set; }
}
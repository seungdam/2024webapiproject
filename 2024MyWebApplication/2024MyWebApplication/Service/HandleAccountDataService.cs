﻿using _2024MyWebApplication.IService;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.PasswordHasher;
using CSCommon;
using Microsoft.Extensions.Options;
using MySqlConnector;
using SqlKata.Compilers;
using SqlKata.Execution;
using StackExchange.Redis;
using System.Data;
using ZLogger;

namespace Com2usProject.Service;

public class HandleAccountDataService : IHandleAccountDataService
{
    readonly ILogger<HandleAccountDataService> _logger;
    readonly IUserAccountDataRepository _account_repos;
    
    MyPasswordHasher _pwhasher;
    public HandleAccountDataService(ILogger<HandleAccountDataService> logger, IUserAccountDataRepository account_repos)
    {
        _logger = logger;
        _account_repos = account_repos;
        _pwhasher = new MyPasswordHasher();
    }

    public async Task<ErrorCode> RegisterAccount(String email, String origin_pw)
    {

        var result_value = await _account_repos.IsExistedAccountData(email);
        var error_code = CSCommon.ErrorCode.ErrorNone;
        if (result_value.ErrorCode == CSCommon.ErrorCode.ErrorNone)
        {
            if (result_value.QueryResult)
            {

                _logger.ZLogInformation($"[HandleAccount.RegisterAccount] ErrorCode: {ErrorCode.RegisterErrorAccountAlreadyExist}, Email: {email} \n");
                return ErrorCode.RegisterErrorAccountAlreadyExist;
            }
        }
        else // 쿼리문에서 에러가 발생한 경우 에러코드 반환
        {
            _logger.ZLogInformation($"[HandleAccount.RegisterAccount] ErrorCode: {ErrorCode.RegisterErrorException}, Email: {email} \n");
            return ErrorCode.RegisterErrorException;
        }

        _pwhasher = new MyPasswordHasher();
        var hashed_pw = _pwhasher.HashingPassword(origin_pw);
        var query_result = await _account_repos.InsertAccountData(email, hashed_pw);

        if(query_result != CSCommon.ErrorCode.ErrorNone)
        {
            _logger.ZLogError($"[HandleAccount.RegisterAccount] ErrorCode: {CSCommon.ErrorCode.RegisterErrorException} ");
            error_code = CSCommon.ErrorCode.RegisterErrorException;
        }

        return error_code;
    }

    public async Task<ErrorCode> VerifyAccount(String email, String reqest_pw)
    {
       
        var account_info = await _account_repos.GetAccountData(email);
        if (account_info is null)
        {
            _logger.ZLogError($"[HandleAccountService.VerifyAccount] ErrorCode: {ErrorCode.LoginErrorNoAccountData}\n");
            return ErrorCode.LoginErrorInvalidAccountData;
        }

        var verify_result = _pwhasher.VerifyPassword(reqest_pw, account_info.UserHashedPassword);
        
        if (verify_result) // 만약 해시 검증에 성공했다면 토큰을 부여하고 
        {
            return ErrorCode.ErrorNone;
        }
        else
        {
            _logger.ZLogError($"[AccountRepository.VerifyAccount] ErrorCode: {ErrorCode.LoginErrorInvalidAccountData}, Email: {email} \n");
            return ErrorCode.LoginErrorInvalidAccountData;
        }
    }

}

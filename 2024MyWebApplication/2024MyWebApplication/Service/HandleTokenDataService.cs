﻿using _2024MyWebApplication.IService;
using _2024MyWebApplication.Model.IRepository;
using CloudStructures.Structures;
using StackExchange.Redis;
using ZLogger;

namespace Com2usProject.Service;


public class HandleTokenDataService : IHandleTokenDataService
{
    readonly IUserTokenDataRepository _token_repos;
    readonly ILogger<HandleTokenDataService> _logger;
    public HandleTokenDataService(ILogger<HandleTokenDataService> logger, IUserTokenDataRepository token_repos)
    {
        _token_repos = token_repos;
        _logger = logger;
    }
    public async Task<bool> RegisterUserAuthToken(String email, String token)
    {
        var result_value = await _token_repos.SetAuthToken(email, token);
        return result_value;
    } 
}

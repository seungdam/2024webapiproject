
using _2024MyWebApplication.IService;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Models.DataStructure;
using ZLogger;


public class HandleCharacterDataService : IHandleCharacterDataService 
{
    readonly ILogger<HandleCharacterDataService> _logger;
    readonly IUserCharacterDataRepository _character_repos;
  
    public HandleCharacterDataService(ILogger<HandleCharacterDataService> logger, IUserCharacterDataRepository character_repos)
    {
        _character_repos = character_repos;
        _logger = logger;
    }

    public async Task<(CSCommon.ErrorCode ErrorCode, CharacterData? ResultData)> LoadMyCharacterData(String email)
    {
        var error_code = CSCommon.ErrorCode.ErrorNone;
        
        try
        {
            var result_value = await _character_repos.GetCharacterDatas(email);
            error_code = result_value.ErrorCode;
            if (error_code != CSCommon.ErrorCode.MySqlQueryErrorException)
            {
                if (result_value.QueryResult is not null)
                {
                    return (ErrorCode: error_code, ResultData: result_value.QueryResult);
                }
                else
                {
                    return (ErrorCode: error_code, ResultData: new CharacterData() { Id = -1, Level = 0, Job = "None" });
                }
            }
            else
            {
                throw new Exception();
            }
        }
        catch
        {
            error_code = CSCommon.ErrorCode.LoadDataExeception;
            _logger.ZLogError($"[InGameRepository.LoadCharacterData] ErrorCode: {error_code}");
            return (ErrorCode: error_code, ResultData: new CharacterData() { Id = -1, Level = 0, Job = "None" });
        }
    }
}

﻿using Microsoft.VisualBasic;
using SqlKata.Execution;
using System.Reflection;
using ZLogger;
using CSCommon;
using System.Transactions;
using _2024MyWebApplication.IService;
using _2024MyWebApplication.Model.IRepository;
using _2024MyWebApplication.Models.DataStructure;


namespace Com2usProject.Service;

public class HandleInventoryDataService: IHandleInventoryDataService
{

    readonly ILogger<HandleCharacterDataService> _logger;
    readonly IUserInventoryDataRepository _inventory_repos;
   
    public HandleInventoryDataService(IUserInventoryDataRepository inventory_respos, ILogger<HandleCharacterDataService> logger)
    {
        _inventory_repos = inventory_respos;
        _logger = logger;
    }
    
   

    public async Task<(CSCommon.ErrorCode ErrorCode, SlotData[]? InventoryData)> LoadInventoryData(Int32 user_id, Int32 inventory_page)
    {

        var error_code = CSCommon.ErrorCode.ErrorNone;
        try
        {
            var repos_reuslt = await _inventory_repos.GetInventoryData(user_id, inventory_page);


            if (repos_reuslt.SlotDatas is not null)
            {
                return (ErrorCode: CSCommon.ErrorCode.ErrorNone, InventoryData: null);
            }
            else
            {
                throw new Exception();
            }
        }
        catch
        {
            _logger.ZLogError($"[HandleInventoryData.LoadInventory] {CSCommon.ErrorCode.LoadDataExeception}");

            return (ErrorCode: CSCommon.ErrorCode.LoadDataExeception, InventoryData: null);
        }
    }


    public async Task<(CSCommon.ErrorCode ErrorCode, SlotData? NewSlotData)> AddItem(int user_id, int item_code, int increase_count)
    {
        try
        {

      
        }
        catch 
        {
            _logger.ZLogError($"[HandleInventoryData.AddItem] ErrorCode {CSCommon.ErrorCode.InventoryServiceException}");
            return (ErrorCode: CSCommon.ErrorCode.InventoryServiceException, NewSlotData: null);
        }
    }


    public async Task<CSCommon.ErrorCode> UserOrDropItem(Int32 user_id, Int32 slot_num, Int32 item_count)
    {
        var error_code = CSCommon.ErrorCode.ErrorNone;
        try
        {
        
        }
        catch
        {
            _logger.ZLogError($"[HandleInventoryData.AddItem] {error_code}");
            return error_code;
        }

        return CSCommon.ErrorCode.ErrorNone;
    }
}

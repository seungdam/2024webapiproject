﻿using System.Net;
using System.Text;
using System.Net.Mime;
using Newtonsoft.Json.Linq;
using _2024MyWebApplication.Model.IRepository;
using static Humanizer.In;

namespace Com2usProject.MiddleWare;

public class MiddleWareTokenVerifier
{
    readonly RequestDelegate _next;
    readonly IUserRequestDataRepository _request_repos;
    readonly IUserTokenDataRepository _token_repos;
    readonly ILogger<MiddleWareTokenVerifier> _logger;
    

    public MiddleWareTokenVerifier(RequestDelegate next, IUserRequestDataRepository request_repos, ILogger<MiddleWareTokenVerifier> logger)
    {
        _next = next;
        _request_repos = request_repos;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {

        try
        {
           
            _logger.LogInformation($"[Request Path] : {context.Request.Path}");
            // 회원 가입을 수행하거나, 로그인 요청을 했을 당시에는 처리하지 않음
            if (context.Request.Path.StartsWithSegments("/register") || context.Request.Path.StartsWithSegments("/login"))
            {
                _logger.LogInformation($"[MiddleWareTokenVerifier] Result: None Check Path");
            }
            else
            {
                // ======= Body 추출 ==========
                var bodyContents = await GetBodyStringFromRequest(context.Request);
                var token = bodyContents["UserToken"].ToString();
                var user_id = bodyContents["UserId"].Value<Int32>();
                var request_type = bodyContents["RequestType"].Value<Int32>();
                _logger.LogInformation($"[MiddleWareTokenVerifier] Token : {token} | UserId: {user_id} | RequsetType : {request_type}");
                var token_check_result = await _token_repos.IsExistAuthToken(token); // 메모리 DB에 등록된 토큰인지 검증
                
                if(!token_check_result.QueryResult) // 검증 실패 시 BadRequest Response 반환
                {
                    _logger.LogInformation($"[MiddleWareTokenVerifier] UnAuthorized Token.");
                    context.Response.Clear();
                    context.Response.StatusCode = (Int32)HttpStatusCode.Unauthorized;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync("Unauthorized Token");
                    return;
                }
                else // 검증된 요청이나 이미 중복 실행중인 상황인지 판단한다.
                {
                    var register_request_result = await _request_repos.SetUserRequestType(user_id, request_type);

                    if (!token_check_result.QueryResult) // 이미 처리 해달라고 등록된 요청이라면?
                    {
                        context.Response.Clear();
                        context.Response.StatusCode = (Int32)HttpStatusCode.BadRequest;
                        context.Response.ContentType = "text/plain";
                        await context.Response.WriteAsync("MultipleRequest Occur");
                        return;
                    }
                }
              
              
                await _next(context);
            }
        }
        catch(Exception e)
        {
            _logger.LogError($"[MiddleWareTokenVerifier] Exception Occur");
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError; // error code 발생
        }
    }

    async Task<JObject> GetBodyStringFromRequest(HttpRequest request)
    {
       
            request.EnableBuffering();
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            //get body string here...
            var requestContents = Encoding.UTF8.GetString(buffer);

            request.Body.Position = 0;  //rewinding the stream to 0
            JObject jsonBody = JObject.Parse(requestContents);
            return jsonBody;
        

    }
  
}

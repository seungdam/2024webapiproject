using _2024MyWebApplication.Model.DBConnectionString;
using System.Data.Common;
using ZLogger;

var builder = WebApplication.CreateBuilder(args);
IConfiguration configuration = builder.Configuration;
builder.Services.Configure<MyDBConnectionString>(configuration.GetSection("DbInformation"));
// Add services to the container.
builder.Services.AddControllers(); // 빌드 이전에 컨트롤러들을 추가한다.
builder.Logging.ClearProviders().AddZLoggerConsole(); // ZLogger 적용


var app = builder.Build();

// ======[Middlewar PipeLine]======
//UserOOO 함수는 미들웨어 파이프라인의 미들웨어 콘텐츠에 대응되는 기능을 수행한다.
app.UseHttpsRedirection();    // http를 https로 리디렉션 수행
// app.UseRouting();          // 라우팅 작업 진행
// app.UseAuthentication();   // 사용자가 WebApi를 호출하기 위한 Authorization & Authentication 작업 수행
// app.UseAuthorization();
//=================================
app.MapControllers();
// Controller가 Endpoint 작업을 수행 -> Request에 관한 Response 반환
// 이런 일련의 과정은 Asp.net core가 web api framework를 기반으로한 Chain of Responsibility를 수행하는 방식을 보여줌.

app.Run(configuration["ServerAddress"]);

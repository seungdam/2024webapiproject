using System;
using System.Reflection.Emit;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;

namespace _2024MyWebApplication.PasswordHasher;

public sealed class MyPasswordHasher
{
    int _salt_bytes_size = 16;
    int _hash_bytes_size = 64;

    public MyPasswordHasher()
    {

    }
    // Salt와 Digest 방법을 혼용해서 사용하자
    public string HashingPassword(string password)
    {
        byte[] saltValue = new byte[_salt_bytes_size]; // 패스워드 해싱에서 사용할 salt 값

        using (var rngCsp = new RNGCryptoServiceProvider())
        {
            rngCsp.GetNonZeroBytes(saltValue); // 0이 아닌 난수값을 할당시켜준다.
        }

        // pbkdf2 라이브러리 함수를 활용해 해싱을 진행
        byte[] hashedValue = KeyDerivation.Pbkdf2(
            password: password,
            salt: saltValue,
            prf: KeyDerivationPrf.HMACSHA512, // SHA256
            iterationCount: 210000, // NIST 기준 210000번이 적당
            numBytesRequested: _hash_bytes_size);

        byte[] finalHashBytes = new byte[_hash_bytes_size + _salt_bytes_size];
        Array.Copy(saltValue, 0, finalHashBytes, 0, _salt_bytes_size);
        Array.Copy(hashedValue, 0, finalHashBytes, _salt_bytes_size, _hash_bytes_size);
        // 앞에 16바이트는 salt 값을 뒤 32바이트는 해쉬된 값을 넣어 DB에 저장. --> 나중에 검증할 때 사용할 것임
        string hashPassword = Convert.ToBase64String(finalHashBytes);
        return hashPassword;
    }

    public bool VerifyPassword(string password, string hashPassword)
    {
        // 데이터베이스에서 해시 패스워드 값을 받아와 이를 저장할 예정
        byte[] hashBytes = Convert.FromBase64String(hashPassword);

        byte[] restoreSaltValue = new byte[_salt_bytes_size];
        byte[] restoreHashValue = new byte[_hash_bytes_size];

        Array.Copy(hashBytes, 0, restoreSaltValue, 0, _salt_bytes_size);
        Array.Copy(hashBytes, _salt_bytes_size, restoreHashValue, 0, _hash_bytes_size);

        byte[] compareHashedValue = KeyDerivation.Pbkdf2(
            password: password,
            salt: restoreSaltValue,
            prf: KeyDerivationPrf.HMACSHA512,
            iterationCount: 210000, 
            numBytesRequested: _hash_bytes_size);

        return restoreHashValue.SequenceEqual(compareHashedValue);

    }


}

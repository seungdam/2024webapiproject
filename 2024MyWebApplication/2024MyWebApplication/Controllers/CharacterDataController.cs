﻿using _2024MyWebApplication.IService;
using _2024MyWebApplication.Models.DataStructure;
using _2024MyWebApplication.Models.ReqResStructure;
using Microsoft.AspNetCore.Mvc;
using ZLogger;

namespace _2024MyWebApplication.Controllers;

public class CharacterDataController : ControllerBase
{
    readonly ILogger<CharacterDataController> _logger;
    readonly IHandleCharacterDataService _character_service;

    public CharacterDataController(ILogger<CharacterDataController> logger, IHandleCharacterDataService character_service)
    {
        _logger = logger;
        _character_service = character_service;
    }
    public async Task<LoadCharacterRes> LoadData(LoadCharacterReq request)
    {
        var response = new LoadCharacterRes();

        var service_result = await _character_service.LoadMyCharacterData(request.UserEmail);
        response.ErrorCode = service_result.ErrorCode;
        response.CharacterData = service_result.ResultData;

        return response;
    }
}

﻿using _2024MyWebApplication.IService;
using _2024MyWebApplication.Models.DataStructure;
using _2024MyWebApplication.Models.ReqResStructure;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
using ZLogger;

namespace _2024MyWebApplication.Controllers;

[ApiController]
[Route("[api/controller]")]
public class InventoryDataController : ControllerBase
{

    readonly IHandleInventoryDataService _inventory_service;
    readonly ILogger<InventoryDataController> _logger;

    public InventoryDataController(ILogger<InventoryDataController> logger, IHandleInventoryDataService iHandler)
    {
        _logger = logger;
        _inventory_service = iHandler;
    }


    [HttpPost("/LoadInventory")]
    public async Task<GetInventoryDataRes> LoadData(GetInventoryDataReq request)
    { 
        GetInventoryDataRes response = new GetInventoryDataRes();
        var service_result = await _inventory_service.LoadInventoryData(request.UserId, request.InventoryPage);

        response.InventoryData = service_result.InventoryData;
        response.ErrorCode = service_result.ErrorCode;
        return response;
    }

    [HttpPost("/UpdateInventory")]
    public async Task<UpdateInventoryDataRes> UpdateData(UpdateInventoryDataReq request)
    {
        var response = new UpdateInventoryDataRes();
        var service_result = await _inventory_service.AddItem(request.UserId, request.ItemCode, request.ItemCount);

        response.ErrorCode = service_result.ErrorCode;
        response.SlotData = service_result.NewSlotData;
        return response;

    }

}

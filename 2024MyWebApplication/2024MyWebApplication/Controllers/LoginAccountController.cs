﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using ZLogger;
using _2024MyWebApplication.Models.ReqResStructure;
using _2024MyWebApplication.IService;
using _2024MyWebApplication.Models.DataStructure;

namespace _2024MyWebApplication.Controllers;

[ApiController]
[Route("api/[controller]")] // 중복되는 접두사에 관해 자동으로 처리 accountdata/get,post....식으로
public class LoginAccountController : ControllerBase
{
    readonly IHandleAccountDataService _account_service;
    readonly IHandleTokenDataService _token_service;
    readonly ILogger<LoginAccountController> _logger;
    
   
    public LoginAccountController(ILogger<LoginAccountController> logger, IHandleAccountDataService account_service, IHandleCharacterDataService character_service,IHandleTokenDataService token_service)
    {
        _logger = logger;
        _account_service = account_service;
        _token_service = token_service;
    }

    [HttpPost()]                          // 어떤 REST API를 사용할 것인가? 헤드를 설정
    [Route("/login")]                     // Request URL의 맨 마지막 부분에 적힐 항목
    public async Task<LoginAccountRes> Login(RegisterAccountReq request)
    {
        LoginAccountRes response = new LoginAccountRes();
       
       var verify_result = await _account_service.VerifyAccount(request.Email, request.Password);

       if (verify_result == CSCommon.ErrorCode.ErrorNone)
       {
           response.UserToken = CreateAuthToken(request.Password);
           var register_token_result = await _token_service.RegisterUserAuthToken(request.Email, response.UserToken);
 
       }
       else
       {
           response.UserToken = "None";
           response.ErrorCode = verify_result;
       }
      
        return response;
    }


    public String CreateAuthToken(String user_password)
    {
        var rng_crpto_provider = new RNGCryptoServiceProvider();
        var new_token = new byte[20];
        rng_crpto_provider.GetNonZeroBytes(new_token);
        var new_authtoken = user_password + Convert.ToBase64String(new_token);
        rng_crpto_provider.Dispose();

       return new_authtoken;
    }
}

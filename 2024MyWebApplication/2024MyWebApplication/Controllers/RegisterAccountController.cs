﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZLogger;
using _2024MyWebApplication.Models.ReqResStructure;
using _2024MyWebApplication.IService;

namespace _2024MyWebApplication.Controllers;


[ApiController]
[Route("api/[controller]")]
public  class RegisterAccountController : ControllerBase 
{
   readonly IHandleAccountDataService _account_service;
   readonly ILogger<RegisterAccountController> _logger;

    public RegisterAccountController(ILogger<RegisterAccountController> logger, IHandleAccountDataService accountDb)
    {
        _logger = logger;
        _account_service = accountDb;
    }

    [HttpPost("/register")]
    public async Task<RegisterAccountRes> Register(RegisterAccountReq request)
    {
        RegisterAccountRes response = new RegisterAccountRes();
        var result_value = await _account_service.RegisterAccount(request.Email, request.Password);
        response.ErrorCode = result_value;
        return response;
    }
    

    

}

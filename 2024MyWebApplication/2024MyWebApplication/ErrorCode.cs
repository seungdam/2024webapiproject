﻿
namespace CSCommon;
 public enum ErrorCode      // Service에서 반환할 에러 타입들을 정의
 {
    ErrorNone = 0,

    //Register 관련
    RegisterErrorAccountAlreadyExist,
    RegisterErrorException,


    // Login 관련
    LoginErrorNoAccountData,
    LoginErrorAlreadyEntered,
    LoginErrorInvalidAccountData,
    LoginErrorException,

    //Query 관련
    MySqlQueryFailed,

    MySqlQueryErrorException,
    RedisQueryErrorException,
    RedisConnErrorException,
    RedisQeryFailed,

    LoadDataExeception,
    InventoryServiceException,
}

public enum RequestTypeAsKey // Redis를 사용해 Rate Limiting 기능을 만들 때 활용 
{
    GetUpdateData,
    UpdateInventoryData,
    DeleteInventoryData,

    GetMailData,
    UpdateMailData,
    DeleteMailData,
}
